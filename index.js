// Comment: single line
/*
	Multi line comment [ctrl/cmd + shft + /]
*/

/*
	Syntax - set of rules that describes the statement must be constructed
	Statements - in progaramming are instructions that we tell the computer to perform. It usually ends with (;)

	ex: console.log("Hello, world!");
*/

console.log("Hello, world!");

// Variables
// It is used to contain data
// Declaring a variable
/*
	let/const varriableName;
*/

let myVariable;
console.log("myVariable");

// console.log(hello);
// let hello;


/*Initializing Variables
	syntax: let/const variableName = value;
*/

let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
const pi = 3.1416;

// Reassignning variable values

productName = "Laptop";
console.log(productName);

// interest = 4.489

// declare b4 inittializing
let supplier;
supplier = "John Smith Tradings";
console.log(supplier);

// Multiple variable declarations
let productCode = "DC017", productBrand = 'Dell';
console.log(productCode,productBrand);

// Variable with reserved keyword(ERR)
/*const let = "Hello";
console.log(let);*/

// Data types
let country = "Philippines";
let province = "Metro Manila";

// Concatenating string
let fullAddress = province +', '+ country
console.log(fullAddress);

let greeting = 'I live in the ' + country;
console.log(greeting);

//escape char
let mailAddress = "Bauan \n Batangas"
console.log(mailAddress);

let	message = 'John\'s employees went home early';
console.log(message);

// numbers
let headcount = 26;
console.log(headcount);

let grade = 88.5;
console.log(grade);

// Exponential
let planetDistance = 2e10;
console.log(planetDistance);

// combining text and numbers
console.log("john's grade last quarter is " + grade);

//Boolean
let isMarried = false;
let isGoodConduct = true;
console.log(isGoodConduct,isMarried);
console.log(isGoodConduct == isMarried);

// Arrays
let grades = [98.2, 93.2, 'asas'];
console.log(grades);

// Objects
/*syntax
	let/const objectName = {
	propertyA: value,
	propertyB: value
	}
*/


let person = {
	fullName: 'Melody Pretty',
	isMarried: false,
	contact: ["1234567", "45632222"],
	address: {
		houseNumber: '234',
		city: "Manila"
	}
};
console.log(person);

let myGrades = {
	firstGrading:98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6
};
console.log(myGrades);

// Null
let spouse = null;
console.log(spouse== null);


// Functions

/*SYNTAX > function functionName(){code area};   */
// function printName(){
// 	console.log("My name is John");
// };
// // Invoke function
// printName();

/* example 1*/
function printName(name){
 	console.log("My name is " + name);
};
// name is argument
printName("John");

// "My favorite animal is" + animal

function printAnimal(animal){
	console.log("My favorite animal is " + animal);
}
printAnimal("Cat");

/* example 2 : function as argument*/
function argumentFucntion(){
	console.log("This fucntion was passed as an argument before the msg was printed");
}

function invokeFunction(argumentFucntion){
	argumentFucntion();
}

invokeFunction(argumentFucntion);

// Creating another function with multiple parameters

function createFullName(firstname, middlename, lastname){
	console.log(firstname+" "+middlename+" "+lastname);
}

createFullName("Donna","G","Saban");

createFullName("Juan",'Diaz',"Dela Cruz","Hello");

// Creating a function using "return" statement
// "return" statement allows the output of a fucntion to be passed to the line/block of code that invoked the function

function returnFullName(firstname, middlename,lastname){
	return firstname+" "+middlename+" "+lastname;
	console.log("This message will not be printed");
}

console.log(returnFullName("Maria", "Clara", "Ibarra"));

let completeName = returnFullName("Maria", "Clara", "Ibarra");
console.log(completeName);